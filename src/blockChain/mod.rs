#[macro_use]
mod block;
pub use block::*;

pub use block::NostagTransaction;
pub use block::DateTime;
pub use block::Utc;

macro_rules! BLOCKCHAIN {
    (default_diff) => (3);
    (default_reward) => (100)
}

#[derive(Debug, Clone)]
pub struct BlockChain
{
    chain: Vec<Block>,
    pending_transactions: Vec<NostagTransaction>,

    diff: u8,
    transaction_reward: u32
}

impl BlockChain
{
    pub fn new(diff: u8, reward:u32) -> BlockChain
    {
        let mut new_chain = Vec::new();

        BlockChain
        {
            chain: new_chain,
            pending_transactions: Vec::new(),
            diff: diff,
            transaction_reward: reward
        }
    }

    pub fn genesis(&mut self) -> Result<(), String>
    {
        match self.chain.len()
        {
            0 => 
            {
                self.chain.push(BLOCK!(genesis));
                return Ok(());
            },
            _ => return Err(String::from("chain is already activated"))
        }
        
        
    }

    pub fn set_diff(&mut self, diff: u8)
    {
        self.diff = diff;
    }
    pub fn get_diff(&self) -> u8
    {
        return self.diff;
    }

    pub fn add_block(&mut self, block: &Block)
    {
        self.chain.push(block.clone());
    }
    fn mine_block(&mut self, block: &Block) -> Result<(), String>
    {
        match self.chain.len()
        {
            0 => return Err(String::from("chain is not activated")),
            _ => 
            {
                let mut new_block = block.clone();
                new_block.set_prev_hash(&self.chain[self.chain.len() - 1].get_hash());

                new_block.mine(self.diff);

                self.chain.push(new_block);
                return Ok(());
            },
        }
    }

    pub fn add_transaction(&mut self, trans: &NostagTransaction) -> Result<(), String>
    {
        
        if trans.get_from() == "" || trans.get_to() == "" {return Err(String::from("invalid transaction"))}
        if !trans.is_valid() {return Err(String::from("invalid transaction"))}

        self.pending_transactions.push(trans.clone());
        return Ok(());
    }

    /*
    you can mine reward transactions forever
    not sure if this is ok...
    */
    pub fn mine_pending_transaction(&mut self, miner: &str) -> Result<Block, String>
    {
        if self.pending_transactions.len() == 0 {return Err(String::from("no pending transactions"));}

        let block = Block::new(&self.pending_transactions);
        let ret = self.mine_block(&block);

        self.pending_transactions = Vec::new();
        self.pending_transactions.push(NostagTransaction::new("", miner, self.transaction_reward as u128, &Utc::now()));
        
        match ret 
        {
            Ok(()) => return Ok(block),
            Err(e) => return Err(e)
        }
    }

    pub fn get_balance(&self, account: &str) -> i128
    {
        let mut balance: i128 = 0;

        for block in self.chain.iter()
        {
            for transaction in block.get_transactions().iter()
            {
                if transaction.get_to() == account
                {
                    balance += transaction.get_amount() as i128;
                }
                else if transaction.get_from() == account
                {
                    balance -= transaction.get_amount() as i128;
                }
            }
        }

        return balance;
    }

    pub fn is_valid(&self) -> bool
    {
        let mut iter = self.chain.iter();
        let first = iter.next().unwrap();

        let mut valid = true;

        iter.fold(first, |prev, val|
        {
            if valid
            {
                if !val.is_valid(self.diff) {valid = false}
                if val.get_hash() != val.calc_hash() { valid = false }
                if val.get_prev_hash() != prev.get_hash() { valid = false }    
            }
            return val
            
        });

        return valid;
    }
}

