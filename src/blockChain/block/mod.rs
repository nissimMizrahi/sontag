extern crate crypto;
pub use crypto::sha2::Sha256;
use crypto::digest::Digest;

mod nostagTransaction;
pub use nostagTransaction::*;

#[macro_export]
macro_rules! BLOCK {
    (genesis) => {
        Block::new(
            &vec![NostagTransaction::new("", "", 0, &Utc.ymd(2019, 7, 12).and_hms(11, 39, 0))]
        )
    };
}


pub fn calc_hash(transaction: &NostagTransaction, hash: &String, nonce: u64) -> String
{
    let string = format!("{}| {}| {}", transaction, hash, nonce);
    let mut seed = Sha256::new();

    seed.input_str(&string);
    return seed.result_str();
}

#[derive(Debug, Clone)]
pub struct Block
{
    transactions: Vec<NostagTransaction>,
    hash: String,
    prev_hash: String,

    nonce: u64
}

impl Block
{
    pub fn new(transactions: &Vec<NostagTransaction>) -> Block
    {
        let hash: String = {
            let mut tmp = String::from("");
            
            for transaction in transactions.iter()
            {
                tmp = calc_hash(transaction, &tmp, 0);
            }
            tmp
        };


        Block
        {
            transactions: transactions.clone(),
            prev_hash: String::from(""),

            nonce: 0,
            hash: hash,
        }
    }

    pub fn get_transactions(&self) -> Vec<NostagTransaction>
    {
        return self.transactions.clone();
    }

    pub fn set_prev_hash(&mut self, hash: &String)
    {
        self.prev_hash = hash.clone();
    }
    pub fn get_prev_hash(&self) -> String
    {
        return self.prev_hash.clone();
    }

    pub fn get_hash(&self) -> String
    {
        return self.hash.clone();
    }

    pub fn calc_hash(&self) -> String
    {
        let hash: String = {
            let mut tmp = self.prev_hash.clone();
            
            for transaction in self.transactions.iter()
            {
                tmp = calc_hash(transaction, &tmp, self.nonce);
            }
            tmp
        };
        return hash;
    }

    pub fn has_valid_hash(&self, diff:u8) -> bool
    {
        return self.hash[..diff as usize] == "0".repeat(diff as usize);
    }
    pub fn has_valid_transactions(&self) -> bool
    {
        for transaction in self.transactions.iter()
        {
            if !transaction.is_valid() {return false}
        }
        return true;
    }
    pub fn is_valid(&self, diff:u8) -> bool
    {
        return self.has_valid_hash(diff) && self.has_valid_transactions();
    }

    pub fn mine(&mut self, diff: u8)
    {
        while !self.has_valid_hash(diff)
        {
            self.nonce += 1;
            self.hash = self.calc_hash();
        }
    }
}