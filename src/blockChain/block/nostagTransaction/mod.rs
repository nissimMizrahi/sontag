extern crate crypto;
pub use crypto::sha2::Sha256;
use crypto::digest::Digest;

extern crate hex;
extern crate secp256k1;
use secp256k1::{Signature, Message};
use secp256k1::key::{SecretKey, PublicKey};

extern crate chrono;
pub use chrono::prelude::*;

use std::fmt;

#[derive(PartialEq, Clone, Debug)]
pub struct NostagTransaction
{
    to: String,
    from: String,
    amount_cents: u128,

    time_stamp: DateTime<Utc>,

    signature: String
}

impl NostagTransaction
{
    pub fn new(from: &str, to: &str, cents: u128, time: &DateTime<Utc>) -> NostagTransaction
    {
        NostagTransaction
        {
            to: to.to_string(),
            from: from.to_string(),
            amount_cents: cents,

            time_stamp: time.clone(),
            signature: String::from("")
        }
    }

    pub fn get_to(&self) -> String
    {
        return self.to.clone();
    }
    pub fn get_from(&self) -> String
    {
        return self.from.clone();
    }
    pub fn get_amount(&self) -> u128
    {
        return self.amount_cents;
    }

    pub fn hash(&self) -> String
    {
        let mut seed = Sha256::new();

        seed.input_str(&format!("{}", self));
        return seed.result_str();
    }

    pub fn sign(&mut self, private_key: &str)
    {
        let context = secp256k1::Secp256k1::new();
        let secret_key = SecretKey::from_slice(&hex::decode(private_key).unwrap()).unwrap();

        let msg = Message::from_slice(&hex::decode(self.hash().as_bytes()).unwrap()).unwrap();
        self.signature = hex::encode(&context.sign(&msg, &secret_key).serialize_der());
    }

    pub fn is_valid(&self) -> bool
    {

        if self.from == "" {return true}
        if self.signature.len() == 0 {return false}

        let context = secp256k1::Secp256k1::new();

        let msg = Message::from_slice(&hex::decode(self.hash().as_bytes()).unwrap()).unwrap();
        let public_key = PublicKey::from_slice(&hex::decode(&self.from).unwrap()).unwrap();
        
        match context.verify(&msg, &Signature::from_der(&hex::decode(&self.signature).unwrap()[..]).unwrap(), &public_key)
        {
            Ok(()) => return true,
            Err(_e) => return false
        }
    }
}

impl fmt::Display for NostagTransaction 
{
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(fmt, "transaction({}: {} --[{}]--> {})", self.time_stamp, self.from, self.amount_cents, self.to)?;
        Ok(())
    }
}