extern crate hex;
extern crate secp256k1;
use secp256k1::{Signature, Message};
use secp256k1::key::{SecretKey, PublicKey};

extern crate rand;
use rand::rngs::OsRng;
extern crate base64;


mod blockChain;
use blockChain::*;

fn new_user() -> (String, String)
{
    let context = secp256k1::Secp256k1::new();

    let (bla_pri, bla_pub) = context.generate_keypair(&mut OsRng::new().unwrap());
    
    //----------------------------------------------secret key---------------------------------------------------
    let my_pri = SecretKey::from_slice(&hex::decode(bla_pri.to_string()).unwrap()).unwrap();
    //----------------------------------------------public key---------------------------------------------------
    let my_pub = PublicKey::from_slice(&hex::decode(bla_pub.to_string()).unwrap()).unwrap();

    return (bla_pri.to_string(), bla_pub.to_string());
}

fn main() {

    let (my_pri, my_pub) = new_user();
    let (other_pri, other_pub) = new_user();

    println!("me - {} | {}\nother - {} | {}", my_pri, my_pub, other_pri, other_pub);

    let mut nostag = BlockChain::new(3, 100);
    nostag.genesis().unwrap();

    let mut trans1 = NostagTransaction::new(&my_pub, &other_pub, 1000, &blockChain::Utc::now());
    
    trans1.sign(&my_pri);
    

    nostag.add_transaction(&trans1);
    nostag.mine_pending_transaction(&my_pub);
    
    trans1 = NostagTransaction::new(&other_pub, &my_pub, 400, &blockChain::Utc::now());
    trans1.sign(&other_pri);
    nostag.add_transaction(&trans1);

    nostag.mine_pending_transaction(&my_pub);

    if nostag.is_valid()
    {
        println!("valid");
    }
    else 
    {
        println!("invalid");
    }

    
    println!("{}", nostag.get_balance(&my_pub));
    
}
